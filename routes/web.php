<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {
	Route::get('/ticket/create', 'TicketController@create')->name('ticket_create');
	Route::post('/ticket/submit', 'TicketController@submit')->name('ticket_submit');
	Route::get('/ticket/my', 'TicketController@my')->name('tickets_my');
	Route::get('/ticket/{id}/show', 'TicketController@show')->name('ticket_show');
	Route::post('/ticket/{id}/comment', 'CommentsController@postComment')->name('ticket_comment');
	Route::post('/ticket/{id}/close', 'TicketController@closeUser')->name('ticket_close');
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function() {
	Route::post('tickets/filter', 'TicketController@filter')->name('admin_tickets_filter');
	Route::get('tickets', 'TicketController@index')->name('admin_tickets');
	Route::post('close/ticket/{id}', 'TicketController@closeAdmin')->name('admin_ticket_close');
	Route::post('assign/ticket/{id}', 'TicketController@assign')->name('admin_ticket_assign');
});
