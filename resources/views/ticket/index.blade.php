@extends('layouts.app')

@section('title', 'Все заявки')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div id="filters" class="card" style="margin-bottom: 30px;">
					<form method="post" action="{{ route('admin_tickets_filter') }}">
						@csrf
						<div class="card-body">
							<h5 class="card-body">Фильтры</h5>
							<div class="row">
								<div class="col-lg-3">
									<label>Просмотренные заявки</label>
									<div class="form-group">
										<input type="checkbox" name="seen" aria-label="Показать просмотренные заявки">
									</div>
								</div>
								<div class="col-lg-3">
									<label>Закрытые заявки</label>
									<div class="form-group">
										<input type="checkbox" name="closed" aria-label="Показать закрытые заявки">
									</div>
								</div>
								<div class="col-lg-3">
									<label>Неотвеченные заявки</label>
									<div class="form-group">
										<input type="checkbox" name="comment" aria-label="Показать неотвеченные заявки">
									</div>
								</div>
							</div>
							<input type="submit" class="btn-info btn" value="Применить">
						</div>

					</form>
				</div>

			</div>


			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">
						<i class="fa fa-ticket"> Заявки</i>
					</div>
					<div class="panel-body">
						@include('includes.flash')
						@if (!count($tickets))
							<p>Заявок нет.</p>
						@else
							<table class="table">
								<thead>
								<tr>
									<th>Создатель</th>
									<th>Дата</th>
									<th>Тема</th>
									<th>Содержание</th>
									<th>Статус</th>
									<th style="text-align:center" colspan="2">Действия</th>
								</tr>
								</thead>
								<tbody>
								@foreach ($tickets as $ticket)
									<tr>
										<td>
											<a href="{{ url('ticket/'. $ticket->id. '/show') }}">
												#{{ $ticket->id }} - {{ $ticket->name }}
											</a>
										</td>
										<td>{{ $ticket->date }}</td>
										<td>{{ $ticket->name }}</td>
										<td>{{ $ticket->body }}</td>
										<td>
											@if ($ticket->status === 'new')
												<span class="label label-success">{{ $ticket->getStatusName() }}</span>
											@elseif ($ticket->status === 'closed')
												<span class="label label-danger">{{ $ticket->getStatusName() }}</span>
											@else
												<span class="label">{{ $ticket->getStatusName() }}</span>
											@endif
										</td>

										<td>
											<a href="{{ url('ticket/' . $ticket->id.'/show') }}" class="btn btn-primary">Комментарий</a>
										</td>
										<td>
											@if ($ticket->status !== 'closed')
												<form action="{{ url('admin/close/ticket/' . $ticket->id) }}"
													  method="POST">
													{!! csrf_field() !!}
													<button type="submit" class="btn btn-danger">Закрыть заявку</button>
												</form>
											@endif
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection