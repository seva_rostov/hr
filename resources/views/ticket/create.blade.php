@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">
					Создание новой заявки
				</h3>
			</div>

			<hr>
			<div class="container-fluid">
				<form enctype="multipart/form-data" action="{{ route('ticket_submit') }}"
					  method="post">

					@csrf
					<div class="form-group">
						<input required class="form-control" name="name" placeholder="Тема *">
					</div>

					<div class="form-group">
						<input required class="form-control" name="text" placeholder="Сообщение *">
					</div>

					<div class="form-group">
						<input class="form-control" type="file" name="file">
					</div>

					<div class="form-group">
						<button type="submit" class="form-control btn btn-info">Отправить</button>
					</div>
				</form>
			</div>
		</div>

	</div>
@endsection
