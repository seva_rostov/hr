@extends('layouts.app')

@section('title', $ticket->name)

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">
						#{{ $ticket->id }} - {{ $ticket->name }}
					</div>

					<div class="panel-body">
						@include('includes.flash')

						<div class="ticket-info">
							<p>{{ $ticket->body }}</p>
							<p>Тема: {{ $ticket->name }}</p>
							<p>
								@if ($ticket->status === 'new')
									Статус: <span class="label label-success">{{ $ticket->getStatusName() }}</span>
								@elseif  ($ticket->status === 'closed')
									Статус: <span class="label label-danger">{{ $ticket->getStatusName() }}</span>
								@else
									Статус: <span class="label">{{ $ticket->getStatusName() }}</span>
								@endif
							</p>
							<p>Дата заявки: {{ $ticket->date }}</p>
						</div>

						<hr>

						<div class="comment-form">
							<form action="{{ route('ticket_comment', ['id' => $ticket->id]) }}" method="POST"
								  class="form">
								{!! csrf_field() !!}

								<input type="hidden" name="ticket_id" value="{{ $ticket->id }}">

								<div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
									<textarea rows="10" id="comment" class="form-control" name="comment"></textarea>

									@if ($errors->has('comment'))
										<span class="help-block">
                                        <strong>{{ $errors->first('comment') }}</strong>
                                    </span>
									@endif
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-primary">Отправить</button>
								</div>
							</form>
						</div>
						<div class="row">
							<div class="col-lg-6">
								@if (($ticket->assigned_user_id !== Auth::user()->id) and (Auth::user()->role == 'ROLE_ADMIN'))
									<form action="{{ route('admin_ticket_assign', ['id' => $ticket->id] ) }}"
										  method="POST">
										{!! csrf_field() !!}
										<button type="submit" class="btn btn-success">Принять к исполнению
										</button>
									</form>
								@endif
									<br>
								@if ($ticket->status !== 'closed')
									<form action="{{ route('ticket_close', ['id' => $ticket->id] ) }}"
										  method="POST">
										{!! csrf_field() !!}
										<button type="submit" class="btn btn-danger">Закрыть заявку</button>
									</form>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="comments">
			@foreach ($comments as $comment)
				<div class="panel panel-@if($ticket->user->id === $comment->user_id) {{"default"}}@else{{"success"}}@endif">
					<div class="panel panel-heading">
						{{ $comment->user->name }}
						<span class="pull-right">{{ $comment->created_at->format('Y-m-d') }}</span>
					</div>

					<div class="panel panel-body">
						{{ $comment->comment }}
					</div>
				</div>
			@endforeach
		</div>

	</div>

@endsection