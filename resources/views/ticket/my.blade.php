@extends('layouts.app')

@section('title', 'Мои заявки')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">
						<i class="fa fa-ticket"> Мои заявки</i>
					</div>

					<div class="panel-body">
						@if ($tickets->isEmpty())
							<p>Вы не создали ни одной заявки.</p>
						@else
							<table class="table">
								<thead>
								<tr>
									<th>#</th>
									<th>Создатель</th>
									<th>Дата</th>
									<th>Тема</th>
									<th>Содержание</th>
									<th>Статус</th>
								</tr>
								</thead>
								<tbody>
								@foreach ($tickets as $item)
									<tr>
										<td>{{ $item->id }}</td>
										<td>{{ $item->user->name }}</td>
										<td>{{ $item->date }}</td>
										<td>{{ $item->name }}</td>
										<td><a href="{{ route('ticket_show', ['id' => $item->id]) }}">{{ $item->body }}</a></td>
										<td>{{ $item->getStatusName()}}</td>
									</tr>
								@endforeach
								</tbody>
							</table>

							{{ $tickets->render() }}
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection