@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
				</div>

				<div class="card-body">
					@if (session('status'))
						<div class="alert alert-success" role="alert">
							{{ session('status') }}
						</div>
					@endif

					<div class="table-responsive table-end">
						<table class="table table-striped">
							<tbody>
							@if(Auth::user()->role == 'ROLE_ADMIN')
								<div class="row ticket_list_btn">
									<div class="col-lg-3">
										<a class="btn btn-info" href="{{route('admin_tickets')}}">Все заявки</a>
									</div>
								</div>
							@else
								<div class="row ticket_list_btn">
									<div class="col-lg-3">
										<a class="btn btn-info" href="{{route('tickets_my')}}">Мои заявки</a>
									</div>
									@if(Auth::user()->canCreate())
									<div class="col-lg-3">
										<a class="btn btn-info" href="{{route('ticket_create')}}">Создать заявку</a>
									</div>
									@endif
								</div>
							@endif
							</tbody>
						</table>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection
