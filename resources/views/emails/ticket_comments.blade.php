<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Обращение</title>
</head>
<body>
<p>
	{{ $comment->comment }}
</p>

---
<p>Ответ от: {{ $user->name }}</p>

<p>Тема: {{ $ticket->name }}</p>
<p>Текст: {{ $ticket->body }}</p>
<p>Статус: {{ $ticket->getStatusName() }}</p>

<p>
	Перейти к обращению: {{ url('ticket/'. $ticket->id.'/show/') }}
</p>

</body>
</html>