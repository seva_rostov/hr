<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Обращение в службу поддержки</title>
</head>
<body>
<p>
	{{ ucfirst($ticketOwner->name) }},
</p>
<p>
	Заявка  #{{ $ticket->id }} успешно закрыта.
</p>

<p>
	Перейти к заявке: {{ url('ticket/'. $ticket->id.'/show') }}
</p>
</body>
</html>