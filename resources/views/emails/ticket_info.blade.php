<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Создана новая заявка</title>
</head>
<body>
<p>
	Спасибо,  {{ ucfirst($user->name) }}, за обращение в службу поддержки. Ваше обращение будет рассмотрено в ближайшее время. Подробнее:
</p>

<p>Тема: {{ $ticket->name }}</p>
<p>Текст: {{ $ticket->body }}</p>
<p>Статус: {{ $ticket->getStatusName() }}</p>

<p>
	Перейти к обращению: {{ url('ticket/'. $ticket->id.'/show') }}
</p>

</body>
</html>