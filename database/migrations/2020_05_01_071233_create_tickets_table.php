<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
			$table->id();
			$table->foreignId('user_id');
			$table->foreignId('assigned_user_id')->nullable();
			$table->date('date');
			$table->string('name');
			$table->boolean('assigned')->nullable();
			$table->boolean('seen')->nullable();
			$table->boolean('admin_comment')->nullable();
			$table->text('body');
			$table->enum('action', ['new', 'comment', 'file', 'assigned', 'closed']);
			$table->enum('status', ['new', 'assigned', 'closed']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
