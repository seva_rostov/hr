# hr

INSTALL
======= 
laravel new hr 
chown -R www-data /var/www/hr

composer require laravel/ui
php artisan ui vue --auth

php artisan serve --port=8082

apt-get install npm
apt-get install node-gyp
apt-get install libssl1.0-dev
apt-get install nodejs-dev
npm install
npm install --save-dev sass fibers
npm run dev
npm run production

create database laravel;
GRANT ALL PRIVILEGES ON laravel.* To 'laravel'@'localhost' IDENTIFIED BY 'laravel';

npm install bootstrap

composer dump-autoload
php artisan db:seed

ADMIN's CREDENTIALS
=================
username: admin
email: admin@admin.ru
password: adminadmin