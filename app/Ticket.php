<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	public $timestamps = false;

	public static $TICKET_NEW    	         = 'new';
	public static $TICKET_COMMENT 	         = 'comment';
	public static $TICKET_CLOSED        	 = 'closed';
	public static $TICKET_FILE       		 = 'file';
	public static $TICKET_ASSIGNED         	 = 'assigned';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'date', 'name', 'body', 'action', 'status', 'user_id', 'assigned_user_id', 'assigned', 'seen', 'admin_comment'
	];

	/**
	 * @return User
	 */
	public function getCreator()
	{
		return $this->user_id;
	}

	/**
	 * @param User $creator
	 */
	public function setCreator($creator)
	{
		$this->user_id = $creator;
	}

	/**
	 * @return User
	 */
	public function getAssigned()
	{
		return $this->assigned_user_id;
	}

	/**
	 * @param User $assigned_user_id
	 */
	public function setAssigned($assigned_user_id)
	{
		$this->assigned_user_id = $assigned_user_id;
	}

	/**
	 * @return boolean
	 */
	public function isAssigned()
	{
		return $this->assigned;
	}

	/**
	 * @param boolean $seen
	 */
	public function setSeen($seen)
	{
		$this->seen = $seen;
	}

	/**
	 * @return boolean
	 */
	public function isSeen()
	{
		return $this->seen;
	}

	/**
	 * @param boolean $assigned
	 */
	public function setIsAssigned($assigned)
	{
		$this->assigned = $assigned;
	}

	/**
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param \DateTime $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * @param string $body
	 */
	public function setBody($body)
	{
		$this->body = $body;
	}

	/**
	 * @return string
	 */
	public function getAction()
	{
		return $this->action;
	}

	/**
	 * @param string $action
	 */
	public function setAction($action)
	{
		$this->action = $action;
	}

	public static function getActionNames()
	{
		return [
			self::$TICKET_NEW     => 'Заявка создана',
			self::$TICKET_COMMENT => 'Оставлен комментарий к заявке',
			self::$TICKET_CLOSED  => 'Заявка закрыта',
		];
	}

	/**
	 * @return string
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public static function getStatusNames()
	{
		return [
			self::$TICKET_NEW      => 'Новая заявка',
			self::$TICKET_ASSIGNED => 'Заявка распределена',
			self::$TICKET_CLOSED   => 'Заявка закрыта',
		];
	}

	/**
	 * @return string
	 */
	public function getStatusName()
	{
		return isset($this->getStatusNames()[$this->status]) ? $this->getStatusNames()[$this->status] : '';
	}

	public function comments()
	{
		return $this->hasMany(Comment::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function assigned_user()
	{
		return $this->belongsTo(User::class);
	}

}
