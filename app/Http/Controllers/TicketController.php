<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Mailers\AppMailer;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Illuminate\Support\Facades\DB;

class TicketController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	/**
	 * Установка значения фильтров
	 */
	public function filter(Request $request)
	{

		$filters['seen'] = $request->get('seen');
		$filters['closed'] = $request->get('closed');
		$filters['comment'] = $request->get('comment');

		return redirect()->route('admin_tickets', ['filters' => $filters]);
	}


	public function index(Request $request)
	{
		$filters[] = $request->get('filters', []);

		if (isset($filters[0]['seen'])) {

			$tickets = Ticket::where(
				'seen', '=', true
			)->paginate(10);
		} elseif (isset($filters[0]['closed'])) {

			$tickets = Ticket::where(
				'status', '=', 'closed'
			)->paginate(10);
		} elseif (isset($filters[0]['comment'])) {

			$tickets = Ticket::where(
				'admin_comment', '=', null
			)->paginate(10);
		} else {
			$tickets = Ticket::paginate(10);
		}

		return view('ticket.index', compact('tickets'));
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function create()
	{
		return view('ticket.create');
	}

	/**
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function submit(Request $request, AppMailer $mailer)
	{
		if (
			!$request->name or
			!$request->text
		) {
			throw new BadRequestHttpException('Неверные параметры');
		}

		/** @var User $user */
		$user = \Auth::user();

		if (!$user->canCreate()) {
			throw new BadRequestHttpException('Не прошло 24 часа с момента регистрации последней заявки!');
		}

		$ticket = new Ticket();

		$ticket->setCreator(\Auth::user()->id);
		$ticket->setDate(new \DateTime());
		$ticket->setName($request->name);
		$ticket->setBody($request->text);
		$ticket->setAction(Ticket::$TICKET_NEW);
		$ticket->setStatus(Ticket::$TICKET_NEW);
		$ticket->save();



		$user->date_ticket_create = new \DateTime();
		$user->save();

		$mailer->sendTicketInformation($user, $ticket);

		return redirect()->route('home')->with("status", "Заявка #$ticket->id создана.");
	}


	public function my()
	{
		$tickets = Ticket::where('user_id', \Auth::user()->id)->paginate(10);

		return view('ticket.my', compact('tickets'));
	}


	public function show($id)
	{
		$ticket = Ticket::where('id', $id)->firstOrFail();

		if (\Auth::user()->role == 'ROLE_ADMIN'){
			$ticket->setSeen(true);
			$ticket->save();
		}

		return view('ticket.show', (['ticket' => $ticket, 'comments' => $ticket->comments]));
	}


	public function closeAdmin($id, AppMailer $mailer)
	{
		$ticket = Ticket::where('id', $id)->firstOrFail();

		$ticket->status = 'closed';

		$ticket->save();

		$ticketOwner = $ticket->user;

		$mailer->sendTicketStatusNotification($ticketOwner, $ticket);

		return redirect()->back()->with("status", 'Заявка #'.$ticket->id.' успешно закрыта.');
	}


	public function closeUser($id, AppMailer $mailer)
	{
		$ticket = Ticket::where('id', $id)->firstOrFail();

		$ticket->status = 'closed';

		$ticket->save();

		if ($ticket->assigned_user_id) {
			$mailer->sendTicketStatusNotification($ticket->assigned_user_id, $ticket);
		}

		return redirect()->back()->with("status", 'Заявка #' . $ticket->id . ' успешно закрыта.');
	}

	public function assign($id, AppMailer $mailer)
	{
		$ticket = Ticket::where('id', $id)->firstOrFail();

		$ticket->assigned_user_id = \Auth::user()->id;

		$ticket->save();

		return redirect()->back()->with("status", 'Заявка #' . $ticket->id . ' успешно принята к исполнению.');
	}

}
