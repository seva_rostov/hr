<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Mailers\AppMailer;
use App\Ticket;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

	public function postComment(Request $request, AppMailer $mailer)
	{
		$this->validate($request, [
			'comment' => 'required',
		]);

		$comment = Comment::create([
			'ticket_id' => $request->input('ticket_id'),
			'user_id'   => \Auth::user()->id,
			'comment'   => $request->input('comment'),
		]);

		$ticket = Ticket::where('id', $request->input('ticket_id'))->firstOrFail();

		if (\Auth::user()->role == 'ROLE_ADMIN') {
			$ticket->admin_comment = true;
			$ticket->save();
		}

		// send mail if the user commenting is not the ticket owner
		if ($comment->ticket->user->id !== \Auth::user()->id) {
			$mailer->sendTicketComments($comment->ticket->user, \Auth::user(), $comment->ticket, $comment);
		}

		return redirect()->back()->with("status", "Комментарий оставлен.");
	}
}
