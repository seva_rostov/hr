<?php
/**
 * Created by:
 * User: svetlanakartysh
 * Date: 02.05.2020
 */

namespace App\Mailers;

use App\Ticket;
use Illuminate\Contracts\Mail\Mailer;


class AppMailer
{
	protected $mailer;
	protected $fromAddress = 'laravel@laravel.ru';
	protected $fromName = 'Laravel';
	protected $to;
	protected $subject;
	protected $view;
	protected $data = [];

	public function __construct(Mailer $mailer)
	{
		$this->mailer = $mailer;
	}

	public function sendTicketInformation($user, Ticket $ticket)
	{
		$this->to = $user->email;
		$this->subject = "[Заявка №: $ticket->id] $ticket->name";
		$this->view = 'emails.ticket_info';
		$this->data = compact('user', 'ticket');

		return $this->deliver();
	}


	public function sendTicketComments($ticketOwner, $user, Ticket $ticket, $comment)
	{
		$this->to      = $ticketOwner->email;
		$this->subject = "RE: $ticket->name (Обращение #: $ticket->id)";
		$this->view    = 'emails.ticket_comments';
		$this->data    = compact('ticketOwner', 'user', 'ticket', 'comment');

		return $this->deliver();
	}


	public function sendTicketStatusNotification($ticketOwner, Ticket $ticket)
	{
		$this->to      = $ticketOwner->email;
		$this->subject = "RE: $ticket->name (Заявка #: $ticket->id)";
		$this->view    = 'emails.ticket_status';
		$this->data    = compact('ticketOwner', 'ticket');

		return $this->deliver();
	}

	public function deliver()
	{
		$this->mailer->send($this->view, $this->data, function($message) {
			$message->from($this->fromAddress, $this->fromName)
				->to($this->to)->subject($this->subject);
		});
	}

}